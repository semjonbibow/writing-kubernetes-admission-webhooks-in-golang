The application has been deployed successfully.

Try creating a VirtualRouter with the following command:

    cat <<EOF | kubectl apply -f -
    apiVersion: appmesh.k8s.aws/v1beta2
    kind: VirtualRouter
    metadata:
      name: arbeitssteuerung2
      namespace: ifp-int-sys
    spec:
      listeners:
      - portMapping:
          port: 8080
          protocol: http
      meshRef:
        name: ifp-iat-zm28w
      routes:
      - httpRoute:
          action:
            weightedTargets:
            - virtualNodeRef:
                name: arbeitssteuerung
                namespace: ifp-int-sys
              weight: 1
          match:
            prefix: /
          timeout:
            perRequest:
              unit: s
              value: 900
        name: main
    EOF