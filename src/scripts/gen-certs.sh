#! /bin/sh
set -uo errexit

export APP="demo-webhook"
export NAMESPACE="default"
export SERVER="${APP}.${NAMESPACE}.svc"

CORPORATION=DB
GROUP=IFP
CITY=Frankfurt
STATE=NRW
COUNTRY=DE

CERT_AUTH_PASS=`openssl rand -base64 32`
echo $CERT_AUTH_PASS > cert_auth_password
CERT_AUTH_PASS=`cat cert_auth_password`

echo "     create the certificate authority"
openssl \
  req \
  -subj "//CN=$SERVER.ca/OU=$GROUP/O=$CORPORATION/L=$CITY/ST=$STATE/C=$COUNTRY" \
  -new \
  -x509 \
  -passout pass:$CERT_AUTH_PASS \
  -keyout ca-cert.key \
  -out ca-cert.crt \
  -days 36500

echo "     create client private key (used to decrypt the cert we get from the CA)"
openssl genrsa -out $SERVER.key

echo "     create the CSR(Certitificate Signing Request)"
cat <<EOF >csr.config
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
prompt = no
[req_distinguished_name]
C = $COUNTRY
CN = $SERVER
OU = $GROUP
O = $CORPORATION
L = $CITY
ST = $STATE
[ v3_req ]
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names
[alt_names]
DNS.1 = ${APP}
DNS.2 = ${APP}.${NAMESPACE}
DNS.3 = ${SERVER}
DNS.4 = ${SERVER}.cluster.local
EOF

openssl \
  req \
  -new \
  -nodes \
  -sha256 \
  -key $SERVER.key \
  -out $SERVER.csr \
  -config csr.config

echo "     sign the certificate with the certificate authority"
cat <<EOF >openssl.cnf
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth, clientAuth
[SAN]
subjectAltName=DNS:${APP},DNS:${APP}.${NAMESPACE},DNS:${SERVER},DNS:${SERVER}.cluster.local
EOF

openssl \
  x509 \
  -req \
  -days 36500 \
  -in $SERVER.csr \
  -CA ca-cert.crt \
  -CAkey ca-cert.key \
  -CAcreateserial \
  -out $SERVER.crt \
  -extfile openssl.cnf \
  -extensions SAN \
  -passin pass:$CERT_AUTH_PASS

kubectl delete secret -n $NAMESPACE $APP || true

kubectl create secret generic -n $NAMESPACE $APP \
  --from-file=key.pem=$SERVER.key \
  --from-file=cert.pem=$SERVER.crt

cat ca-cert.crt | base64