package virtualrouter_test

import (
	"context"
	"ifp-sit-kubewebhooks/internal/mutation/virtualrouter"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/aws/aws-app-mesh-controller-for-k8s/apis/appmesh/v1beta2"
)

func TestVirtualRouterMutation(t *testing.T) {
	defaultBackendNamespace := "ifp-sit"
	defaultBackendName := "default-backend"
	testPath1 := "/actuator"
	testPath2 := "/test123"
	grpcMethod := "GET"

	tests := map[string]struct {
		forbiddenEndpoints []string
		obj                *v1beta2.VirtualRouter
		expObj             *v1beta2.VirtualRouter
	}{
		"VirtualRouter should be modifiable with one forbiddenEndpoint": {
			forbiddenEndpoints: []string{testPath1},
			obj: &v1beta2.VirtualRouter{
				Spec: v1beta2.VirtualRouterSpec{
					Routes: []v1beta2.Route{},
				},
			},
			expObj: &v1beta2.VirtualRouter{
				Spec: v1beta2.VirtualRouterSpec{
					Routes: []v1beta2.Route{
						{
							Name: "deny0",
							HTTPRoute: &v1beta2.HTTPRoute{
								Match: v1beta2.HTTPRouteMatch{
									Prefix: &testPath1,
								},
								Action: v1beta2.HTTPRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      defaultBackendName,
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
		"VirtualRouter should be modifiable with multiple forbiddenEndpoint": {
			forbiddenEndpoints: []string{testPath1, testPath2},
			obj: &v1beta2.VirtualRouter{
				Spec: v1beta2.VirtualRouterSpec{
					Routes: []v1beta2.Route{},
				},
			},
			expObj: &v1beta2.VirtualRouter{
				Spec: v1beta2.VirtualRouterSpec{
					Routes: []v1beta2.Route{
						{
							Name: "deny0",
							HTTPRoute: &v1beta2.HTTPRoute{
								Match: v1beta2.HTTPRouteMatch{
									Prefix: &testPath1,
								},
								Action: v1beta2.HTTPRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      defaultBackendName,
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
						{
							Name: "deny1",
							HTTPRoute: &v1beta2.HTTPRoute{
								Match: v1beta2.HTTPRouteMatch{
									Prefix: &testPath2,
								},
								Action: v1beta2.HTTPRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      defaultBackendName,
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
		"VirtualRouter existing routes should not be removed": {
			forbiddenEndpoints: []string{testPath1},
			obj: &v1beta2.VirtualRouter{
				Spec: v1beta2.VirtualRouterSpec{
					Routes: []v1beta2.Route{
						{
							Name: "kapaservice",
							HTTPRoute: &v1beta2.HTTPRoute{
								Match: v1beta2.HTTPRouteMatch{
									Prefix: &testPath2,
								},
								Action: v1beta2.HTTPRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      "test123",
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
					},
				},
			},
			expObj: &v1beta2.VirtualRouter{
				Spec: v1beta2.VirtualRouterSpec{
					Routes: []v1beta2.Route{
						{
							Name: "deny0",
							HTTPRoute: &v1beta2.HTTPRoute{
								Match: v1beta2.HTTPRouteMatch{
									Prefix: &testPath1,
								},
								Action: v1beta2.HTTPRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      defaultBackendName,
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
						{
							Name: "kapaservice",
							HTTPRoute: &v1beta2.HTTPRoute{
								Match: v1beta2.HTTPRouteMatch{
									Prefix: &testPath2,
								},
								Action: v1beta2.HTTPRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      "test123",
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
		"Not crash if not using httproutes": {
			forbiddenEndpoints: []string{testPath1},
			obj: &v1beta2.VirtualRouter{
				Spec: v1beta2.VirtualRouterSpec{
					Routes: []v1beta2.Route{
						{
							Name: "kapaservice",
							GRPCRoute: &v1beta2.GRPCRoute{
								Match: v1beta2.GRPCRouteMatch{
									MethodName: &grpcMethod,
								},
								Action: v1beta2.GRPCRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      "test123",
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
					},
				},
			},
			expObj: &v1beta2.VirtualRouter{
				Spec: v1beta2.VirtualRouterSpec{
					Routes: []v1beta2.Route{
						{
							Name: "deny0",
							HTTPRoute: &v1beta2.HTTPRoute{
								Match: v1beta2.HTTPRouteMatch{
									Prefix: &testPath1,
								},
								Action: v1beta2.HTTPRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      defaultBackendName,
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
						{
							Name: "kapaservice",
							GRPCRoute: &v1beta2.GRPCRoute{
								Match: v1beta2.GRPCRouteMatch{
									MethodName: &grpcMethod,
								},
								Action: v1beta2.GRPCRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      "test123",
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
		"VirtualRouter should still be valid if editing prefix paths": {
			forbiddenEndpoints: []string{testPath1},
			obj: &v1beta2.VirtualRouter{
				Spec: v1beta2.VirtualRouterSpec{
					Routes: []v1beta2.Route{
						{
							Name: "deny0",
							HTTPRoute: &v1beta2.HTTPRoute{
								Match: v1beta2.HTTPRouteMatch{
									Prefix: &testPath2,
								},
								Action: v1beta2.HTTPRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      defaultBackendName,
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
						{
							Name: "kapaservice",
							GRPCRoute: &v1beta2.GRPCRoute{
								Match: v1beta2.GRPCRouteMatch{
									MethodName: &grpcMethod,
								},
								Action: v1beta2.GRPCRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      "test123",
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
					},
				},
			},
			expObj: &v1beta2.VirtualRouter{
				Spec: v1beta2.VirtualRouterSpec{
					Routes: []v1beta2.Route{
						{
							Name: "deny1",
							HTTPRoute: &v1beta2.HTTPRoute{
								Match: v1beta2.HTTPRouteMatch{
									Prefix: &testPath1,
								},
								Action: v1beta2.HTTPRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      defaultBackendName,
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
						{
							Name: "deny0",
							HTTPRoute: &v1beta2.HTTPRoute{
								Match: v1beta2.HTTPRouteMatch{
									Prefix: &testPath2,
								},
								Action: v1beta2.HTTPRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      defaultBackendName,
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
						{
							Name: "kapaservice",
							GRPCRoute: &v1beta2.GRPCRoute{
								Match: v1beta2.GRPCRouteMatch{
									MethodName: &grpcMethod,
								},
								Action: v1beta2.GRPCRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      "test123",
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
		"VirtualRouter existing forbidden endpoints should not be added": {
			forbiddenEndpoints: []string{testPath1},
			obj: &v1beta2.VirtualRouter{
				Spec: v1beta2.VirtualRouterSpec{
					Routes: []v1beta2.Route{
						{
							Name: "kapaservice",
							HTTPRoute: &v1beta2.HTTPRoute{
								Match: v1beta2.HTTPRouteMatch{
									Prefix: &testPath1,
								},
								Action: v1beta2.HTTPRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      "test123",
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
					},
				},
			},
			expObj: &v1beta2.VirtualRouter{
				Spec: v1beta2.VirtualRouterSpec{
					Routes: []v1beta2.Route{
						{
							Name: "kapaservice",
							HTTPRoute: &v1beta2.HTTPRoute{
								Match: v1beta2.HTTPRouteMatch{
									Prefix: &testPath1,
								},
								Action: v1beta2.HTTPRouteAction{
									WeightedTargets: []v1beta2.WeightedTarget{
										{
											Weight: 1,
											VirtualNodeRef: &v1beta2.VirtualNodeReference{
												Name:      "test123",
												Namespace: &defaultBackendNamespace,
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			assert := assert.New(t)
			require := require.New(t)

			m := virtualrouter.NewVirtualRouterSafer(defaultBackendNamespace, defaultBackendName, test.forbiddenEndpoints)

			err := m.EnsureSafetyVR(context.TODO(), test.obj)
			require.NoError(err)

			assert.Equal(test.expObj, test.obj)
		})
	}
}
