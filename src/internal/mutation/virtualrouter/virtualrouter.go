package virtualrouter

import (
	"context"
	"fmt"
	"regexp"
	"strconv"

	"github.com/aws/aws-app-mesh-controller-for-k8s/apis/appmesh/v1beta2"
	"golang.org/x/exp/slices"
)

type VirtualRouterSafer interface {
	EnsureSafetyVR(ctx context.Context, vr *v1beta2.VirtualRouter) error
}

type virtualRouterSafer struct {
	defaultBackendName string
	defaultBackendNamespace string
	forbiddenEndpoints []string
}

func NewVirtualRouterSafer(defaultBackendNamespace string, defaultBackendName string, forbiddenEndpoints []string) VirtualRouterSafer {
	return virtualRouterSafer {
		defaultBackendNamespace: defaultBackendNamespace,
		defaultBackendName: defaultBackendName,
		forbiddenEndpoints: forbiddenEndpoints,
	}
}

func (v virtualRouterSafer) EnsureSafetyVR(_ context.Context, vr *v1beta2.VirtualRouter) error {
	existingRoutes := make([]string, 0)
	highestDenyIndex := -1
	
	for _, r := range vr.Spec.Routes {
		if r.HTTPRoute != nil && r.HTTPRoute.Match.Prefix != nil {
			existingRoutes = append(existingRoutes, *r.HTTPRoute.Match.Prefix)
		}
		isDeny, _ := regexp.MatchString("^deny[0-9]+$", r.Name)
		if isDeny {
			indexStr := r.Name[4:]
			index, _ := strconv.Atoi(indexStr)
			if index > highestDenyIndex {
				highestDenyIndex = index
			}
		}
	}

	routesToAdd := make([]string, 0)

	for _, e := range v.forbiddenEndpoints {
		if ! slices.Contains(existingRoutes, e) {
			routesToAdd = append(routesToAdd, e)
		}
	}

	routes := make([]v1beta2.Route, 0, len(routesToAdd) + len(vr.Spec.Routes))

	for i := range routesToAdd {
		route := v1beta2.Route{
			Name: fmt.Sprintf("deny%d", i + highestDenyIndex + 1),
			HTTPRoute: &v1beta2.HTTPRoute{
				Match: v1beta2.HTTPRouteMatch{
					Prefix: &routesToAdd[i],
				},
				Action: v1beta2.HTTPRouteAction{
					WeightedTargets: []v1beta2.WeightedTarget{
						{
							Weight: 1,
							VirtualNodeRef: &v1beta2.VirtualNodeReference{
								Namespace: &v.defaultBackendNamespace,
								Name: v.defaultBackendName,
							},
						},
					},
				},
			},
		}
		routes = append(routes, route)
	}

	for _, r := range vr.Spec.Routes {
		routes = append(routes, r)
	}

	vr.Spec.Routes = routes

	return nil
}

// DummyVirtualRouterSafer is a VirtualRouterSafer that doesn't do anything.
const DummyVirtualRouterSafer = dummyVirtualRouterSafer(0)

type dummyVirtualRouterSafer int

var _ dummyVirtualRouterSafer = DummyVirtualRouterSafer

func (dummyVirtualRouterSafer) EnsureSafetyVR(ctx context.Context, vr *v1beta2.VirtualRouter) error {
	return nil
}
