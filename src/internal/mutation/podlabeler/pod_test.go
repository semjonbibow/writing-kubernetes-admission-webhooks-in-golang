package podlabeler_test

import (
	"context"
	podlabeler "ifp-sit-kubewebhooks/internal/mutation/podlabeler"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestVirtualRouterMutation(t *testing.T) {
	tests := map[string]struct {
		obj    *corev1.Pod
		expObj *corev1.Pod
	}{
		"Pod should have new Label mutated": {
			obj: &corev1.Pod{
				ObjectMeta: v1.ObjectMeta{},
			},
			expObj: &corev1.Pod{
				ObjectMeta: v1.ObjectMeta{
					Labels: map[string]string{
						"mutated": "true",
					},
				},
			},
		},
		"Pod should modity existing Label mutated": {
			obj: &corev1.Pod{
				ObjectMeta: v1.ObjectMeta{
					Labels: map[string]string{
						"mutated": "false",
						"other":   "label",
					},
				},
			},
			expObj: &corev1.Pod{
				ObjectMeta: v1.ObjectMeta{
					Labels: map[string]string{
						"mutated": "true",
						"other":   "label",
					},
				},
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			assert := assert.New(t)
			require := require.New(t)

			m := podlabeler.NewPodLabeled()

			err := m.EnsureLabelPod(context.TODO(), test.obj)
			require.NoError(err)

			assert.Equal(test.expObj, test.obj)
		})
	}
}
