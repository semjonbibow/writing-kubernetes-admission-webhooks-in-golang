package podlabeler

import (
	"context"

	corev1 "k8s.io/api/core/v1"
)

type PodLabeled interface {
	EnsureLabelPod(ctx context.Context, pod *corev1.Pod) error
}

type podLabeled struct {
}

func NewPodLabeled() PodLabeled {
	return podLabeled {}
}

func (p podLabeled) EnsureLabelPod(_ context.Context, pod *corev1.Pod) error {
	if pod.ObjectMeta.Labels == nil {
		pod.ObjectMeta.Labels = make(map[string]string)
	}
	pod.ObjectMeta.Labels["mutated"] = "true"
	return nil
}

// DummyPodLabeled is a PodLabeled that doesn't do anything.
const DummyPodLabeled = dummyPodLabeled(0)

type dummyPodLabeled int

var _ dummyPodLabeled = DummyPodLabeled

func (dummyPodLabeled) EnsureSafetyVR(ctx context.Context, pod *corev1.Pod) error {
	return nil
}
