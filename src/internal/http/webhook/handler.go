package webhook

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	appmeshv1beta2 "github.com/aws/aws-app-mesh-controller-for-k8s/apis/appmesh/v1beta2"
	kwhhttp "github.com/slok/kubewebhook/v2/pkg/http"
	kwhlog "github.com/slok/kubewebhook/v2/pkg/log"
	kwhmodel "github.com/slok/kubewebhook/v2/pkg/model"
	kwhwebhook "github.com/slok/kubewebhook/v2/pkg/webhook"
	kwhmutating "github.com/slok/kubewebhook/v2/pkg/webhook/mutating"
	kwhvalidating "github.com/slok/kubewebhook/v2/pkg/webhook/validating"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"ifp-sit-kubewebhooks/internal/log"
	"ifp-sit-kubewebhooks/internal/validation/ingress"
)

// kubewebhookLogger is a small proxy to use our logger with Kubewebhook.
type kubewebhookLogger struct {
	log.Logger
}

func (l kubewebhookLogger) WithValues(kv map[string]interface{}) kwhlog.Logger {
	return kubewebhookLogger{Logger: l.Logger.WithKV(kv)}
}
func (l kubewebhookLogger) WithCtxValues(ctx context.Context) kwhlog.Logger {
	return l.WithValues(kwhlog.ValuesFromCtx(ctx))
}
func (l kubewebhookLogger) SetValuesOnCtx(parent context.Context, values map[string]interface{}) context.Context {
	return kwhlog.CtxWithValues(parent, values)
}

// ingressValidation sets up the webhook handler for validating an ingress using a chain of validations.
// Thec validation chain will check first if the ingress has a single host, if not it will stop the
// validation chain, otherwirse it will check the nest ingress Validator that will try matching the host
// with allowed host.
func (h handler) ingressValidation() (http.Handler, error) {
	// Single host validator.
	vSingle := kwhvalidating.ValidatorFunc(func(ctx context.Context, ar *kwhmodel.AdmissionReview, obj metav1.Object) (*kwhvalidating.ValidatorResult, error) {
		err := h.ingSingleHostVal.Validate(ctx, obj)
		if err != nil {
			if errors.Is(err, ingress.ErrNotIngress) {
				h.logger.Warningf("received object is not an ingress")
				return &kwhvalidating.ValidatorResult{Valid: true}, nil
			}

			return &kwhvalidating.ValidatorResult{
				Message: fmt.Sprintf("ingress is invalid: %s", err),
				Valid:   false,
			}, nil
		}

		return &kwhvalidating.ValidatorResult{Valid: true}, nil
	})

	// Host based on regex validator.
	vRegex := kwhvalidating.ValidatorFunc(func(ctx context.Context, ar *kwhmodel.AdmissionReview, obj metav1.Object) (*kwhvalidating.ValidatorResult, error) {
		err := h.ingRegexHostVal.Validate(ctx, obj)
		if err != nil {
			if errors.Is(err, ingress.ErrNotIngress) {
				h.logger.Warningf("received object is not an ingress")
				return &kwhvalidating.ValidatorResult{Valid: true}, nil
			}

			return &kwhvalidating.ValidatorResult{
				Message: fmt.Sprintf("ingress host is invalid: %s", err),
				Valid:   false,
			}, nil
		}

		return &kwhvalidating.ValidatorResult{Valid: true}, nil
	})

	logger := kubewebhookLogger{Logger: h.logger.WithKV(log.KV{"lib": "kubewebhook", "webhook": "ingressValidation"})}

	// Create a chain with both ingress validations and use these to create the kwhwebhook.
	v := kwhvalidating.NewChain(logger, vSingle, vRegex)
	wh, err := kwhvalidating.NewWebhook(kwhvalidating.WebhookConfig{
		ID:        "ingressValidation",
		Validator: v,
		Logger:    logger,
	})
	if err != nil {
		return nil, fmt.Errorf("could not create webhook: %w", err)
	}

	whHandler, err := kwhhttp.HandlerFor(kwhhttp.HandlerConfig{
		Webhook: kwhwebhook.NewMeasuredWebhook(h.metrics, wh),
		Logger:  logger,
	})
	if err != nil {
		return nil, fmt.Errorf("could not create handler from webhook: %w", err)
	}

	return whHandler, nil
}

// safeVirtualRouter sets up the webhook handler to set safety VirtualRouter settings.
func (h handler) safeVirtualRouter() (http.Handler, error) {
	mt := kwhmutating.MutatorFunc(func(ctx context.Context, ar *kwhmodel.AdmissionReview, obj metav1.Object) (*kwhmutating.MutatorResult, error) {
		vr, ok := obj.(*appmeshv1beta2.VirtualRouter)
		if !ok {
			h.logger.Warningf("received object is not an appmeshv1beta2.VirtualRouter")
			return &kwhmutating.MutatorResult{}, nil
		}

		err := h.virtRouteSafer.EnsureSafetyVR(ctx, vr)
		if err != nil {
			return nil, fmt.Errorf("could not set safety settings on virtual router: %w", err)
		}

		return &kwhmutating.MutatorResult{MutatedObject: vr}, nil
	})

	logger := kubewebhookLogger{Logger: h.logger.WithKV(log.KV{"lib": "kubewebhook", "webhook": "safeVirtualRouter"})}

	// Create a static webhook, placing the specific object we are going to redeive, this is important
	// so we receive a CR instead of `runtume.Unstructured` on the mutator.
	wh, err := kwhmutating.NewWebhook(kwhmutating.WebhookConfig{
		ID:      "safeServiceMonitor",
		Obj:     &appmeshv1beta2.VirtualRouter{},
		Mutator: mt,
		Logger:  logger,
	})
	if err != nil {
		return nil, fmt.Errorf("could not create webhook: %w", err)
	}

	whHandler, err := kwhhttp.HandlerFor(kwhhttp.HandlerConfig{
		Webhook: kwhwebhook.NewMeasuredWebhook(h.metrics, wh),
		Logger:  logger,
	})
	if err != nil {
		return nil, fmt.Errorf("could not create handler from webhook: %w", err)
	}

	return whHandler, nil
}

func (h handler) podLabeler() (http.Handler, error) {
	mt := kwhmutating.MutatorFunc(func(ctx context.Context, ar *kwhmodel.AdmissionReview, obj metav1.Object) (*kwhmutating.MutatorResult, error) {
		pod, ok := obj.(*corev1.Pod)
		if !ok {
			h.logger.Warningf("received object is not an corev1.Pod")
			return &kwhmutating.MutatorResult{}, nil
		}

		err := h.podLabeled.EnsureLabelPod(ctx, pod)
		if err != nil {
			return nil, fmt.Errorf("could not set label on pod: %w", err)
		}

		return &kwhmutating.MutatorResult{MutatedObject: pod}, nil
	})

	logger := kubewebhookLogger{Logger: h.logger.WithKV(log.KV{"lib": "kubewebhook", "webhook": "podLabeler"})}

	// Create a static webhook, placing the specific object we are going to redeive, this is important
	// so we receive a CR instead of `runtume.Unstructured` on the mutator.
	wh, err := kwhmutating.NewWebhook(kwhmutating.WebhookConfig{
		ID:      "podLabeler",
		Obj:     &corev1.Pod{},
		Mutator: mt,
		Logger:  logger,
	})
	if err != nil {
		return nil, fmt.Errorf("could not create webhook: %w", err)
	}

	whHandler, err := kwhhttp.HandlerFor(kwhhttp.HandlerConfig{
		Webhook: kwhwebhook.NewMeasuredWebhook(h.metrics, wh),
		Logger:  logger,
	})
	if err != nil {
		return nil, fmt.Errorf("could not create handler from webhook: %w", err)
	}

	return whHandler, nil
}
