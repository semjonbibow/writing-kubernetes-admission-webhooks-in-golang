package webhook

import (
	"net/http"
)

// routes wires the routes to handlers on a specific router.
func (h handler) routes(router *http.ServeMux) error {
	ingressVal, err := h.ingressValidation()
	if err != nil {
		return err
	}
	router.Handle("/wh/validating/ingress", ingressVal)

	safeVirtualRouter, err := h.safeVirtualRouter()
	if err != nil {
		return err
	}
	router.Handle("/wh/mutating/safevirtualrouter", safeVirtualRouter)

	podLabeler, err := h.podLabeler()
	if err != nil {
		return err
	}
	router.Handle("/wh/mutating/podlabeler", podLabeler)

	return nil
}
