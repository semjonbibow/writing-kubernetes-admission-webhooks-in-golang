package webhook

import (
	"fmt"
	"net/http"

	"ifp-sit-kubewebhooks/internal/log"
	"ifp-sit-kubewebhooks/internal/mutation/podlabeler"
	"ifp-sit-kubewebhooks/internal/mutation/virtualrouter"
	"ifp-sit-kubewebhooks/internal/validation/ingress"
)

// Config is the handler configuration.
type Config struct {
	MetricsRecorder            MetricsRecorder
	IngressRegexHostValidator  ingress.Validator
	IngressSingleHostValidator ingress.Validator
	VirtualRouterSafer         virtualrouter.VirtualRouterSafer
	PodLabeled                 podlabeler.PodLabeled
	Logger                     log.Logger
}

func (c *Config) defaults() error {

	if c.IngressRegexHostValidator == nil {
		return fmt.Errorf("ingress regex host validator is required")
	}

	if c.IngressSingleHostValidator == nil {
		return fmt.Errorf("ingress single host validator is required")
	}

	if c.VirtualRouterSafer == nil {
		return fmt.Errorf("ingress regex host validator is required")
	}

	if c.PodLabeled == nil {
		return fmt.Errorf("pod mutator is required")
	}

	if c.MetricsRecorder == nil {
		c.MetricsRecorder = dummyMetricsRecorder
	}

	if c.Logger == nil {
		c.Logger = log.Dummy
	}

	return nil
}

type handler struct {
	ingRegexHostVal  ingress.Validator
	ingSingleHostVal ingress.Validator
	virtRouteSafer   virtualrouter.VirtualRouterSafer
	podLabeled       podlabeler.PodLabeled
	handler          http.Handler
	metrics          MetricsRecorder
	logger           log.Logger
}

// New returns a new webhook handler.
func New(config Config) (http.Handler, error) {
	err := config.defaults()
	if err != nil {
		return nil, fmt.Errorf("handler configuration is not valid: %w", err)
	}

	mux := http.NewServeMux()

	h := handler{
		handler:          mux,
		ingRegexHostVal:  config.IngressRegexHostValidator,
		ingSingleHostVal: config.IngressSingleHostValidator,
		virtRouteSafer:   config.VirtualRouterSafer,
		podLabeled:       config.PodLabeled,
		metrics:          config.MetricsRecorder,
		logger:           config.Logger.WithKV(log.KV{"service": "webhook-handler"}),
	}

	// Register all the routes with our router.
	err = h.routes(mux)
	if err != nil {
		return nil, fmt.Errorf("could not register routes on handler: %w", err)
	}

	// Register root handler middlware.
	h.handler = h.measuredHandler(h.handler) // Add metrics middleware.

	return h, nil
}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.handler.ServeHTTP(w, r)
}
