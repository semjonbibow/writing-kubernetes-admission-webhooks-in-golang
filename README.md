## Kubernetes Admission Controllers written in Golang

Mutating Webhooks and Validating Webhooks

---

## Contents

Overview of Kubernetes Admission Controllers

Installing `go`

Using the production ready templates

Demo

----

### Prerequisites

Some understanding of Kubernetes

Some programming experience

---

## About Kubernetes Webhooks

----

### How Kubernetes processes requests

![Kubernetes API Admission Controller
Phases](./images/admission-controller-phases.png)

<small>Source:
<https://kubernetes.io/blog/2019/03/21/a-guide-to-kubernetes-admission-controllers/></small>

Note: You might already know some Admission Controllers: Limit Ranges also
modify objects and check for too much memory usage. Some more admission controllers are used internally. The
NamespaceLifecycle Controller prohibits creation of new resources in namespaces
that are Terminating

----

```yaml
apiVersion: admissionregistration.k8s.io/v1
kind: ValidatingWebhookConfiguration
metadata:
  name: my-validating-webhook
webhooks:
  - name: image-validator.example.com
    admissionReviewVersions: ["v1"]
    sideEffects: None
    clientConfig:
      url: https://example.com/wh/validator/images
      caBundle: <BASE64ENCODED>
    rules:
      - operations: ["CREATE", "UPDATE"]
        apiGroups: [""]
        apiVersions: ["v1"]
        resources: ["pods"]
```

Note: apiGroup, apiVersion and resources all according to `kubectl
api-resources` (also resources must be plural). caBundle is a pain to create.
Operation is CREATE, UPDATE, DELETE or CONNECT

----

```yaml
apiVersion: admissionregistration.k8s.io/v1
kind: MutatingWebhookConfiguration
metadata:
  name: my-mutating-webhook
webhooks:
  - name: labeler.example.com
    admissionReviewVersions: ["v1"]
    sideEffects: None
    clientConfig:
      url: https://example.com/wh/mutating/labeler
      caBundle: <BASE64ENCODED>
    rules:
      - operations: ["CREATE", "UPDATE"]
        apiGroups: ["*"]
        apiVersions: ["*"]
        resources: ["*"]
```

----

### The request to the webhook

```yaml
apiVersion: admission.k8s.io/v1
kind: AdmissionReview
request:
  uid: 705ab4f5-6393-11e8-b7cc-42010a800002
  resource:
    group: ""
    version: v1
    resource: pods
  name: my-pod
  namespace: default
  operation: CREATE
  [...]
```

<small> See also:
<https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/#request> </small>

----

### The response from the webhook

```json
{
  "apiVersion": "admission.k8s.io/v1",
  "kind": "AdmissionReview",
  "response": {
    "uid": "705ab4f5-6393-11e8-b7cc-42010a800002",
    "allowed": true
  }
}
```

<small> See also:
<https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/#response>
</small>

----

### Recap

- You can hook into the creation of any Kubernetes object (even custom resources)
- Certificates are a pain to create

---

## Installing `go`

Visit <https://go.dev/dl/> and select the correct package for your OS

----

![Windows UAM Message](./images/windows-uam.png)

----

### Installing `go` on Windows without Admin rights

- Download the zip file (eg. <https://go.dev/dl/go1.20.6.windows-amd64.zip>)
- Extract it to `~/go_bin`
- Create folder `~/go`
- Set Environment variables
  - `GO_ROOT` to `~/go_bin/go`
  - `GOPATH` to `~/go`
  - Add `~/go_bin/go/bin` to `PATH`
- Test with `go version`

---

## Using production ready templates for creating Kubernetes Admission Controllers

Basically following <https://github.com/slok/k8s-webhook-example>

----

Repository structure:

```bash
.
├── go.mod
├── go.sum
├── cmd
│   └── k8s-webhook-example
│       ├── config.go
│       └── main.go
└── internal
    ├── mutation
    │   ├── mutator1
    │   │   └── mutator1.go
    └── validation
        └── validator1
            └── validator1.go

```

----

Example mutator:

```go
func (p podLabeled) EnsureLabelPod(pod *corev1.Pod) error {
	if pod.ObjectMeta.Labels == nil {
		pod.ObjectMeta.Labels = make(map[string]string)
	}
	pod.ObjectMeta.Labels["mutated"] = "true"
	return nil
}

```

----

Tests:

```go
obj: &corev1.Pod{
  ObjectMeta: v1.ObjectMeta{},
},
expObj: &corev1.Pod{
  ObjectMeta: v1.ObjectMeta{
    Labels: map[string]string{
      "mutated": "true",
    },
  },
},
```

```sh
$ go test internal/mutation/podlabeler
ok      internal/mutation/podlabeler       0.228s
```

----

Tests:

```go
obj: &corev1.Pod{
  ObjectMeta: v1.ObjectMeta{
    Labels: map[string]string{
      "mutated": "false",
      "other":   "label",
    },
  },
},
expObj: &corev1.Pod{
  ObjectMeta: v1.ObjectMeta{
    Labels: map[string]string{
      "mutated": "true",
      "other":   "label",
    },
  },
},
```

```sh
$ go test internal/mutation/podlabeler
ok      internal/mutation/podlabeler       0.321s
```

---

## Demo

---

## Recap

- Kubernetes Webhooks are a powerful way to extend Kubernetes
- Existing solutions make Admission Webhooks (relatively) easy to use
- Don't overcomplicate things ([OPA Gatekeeper](https://open-policy-agent.github.io/gatekeeper/website/) might be enough)